//
//  SubmitRequstVC.m
//  PianoClubHouse
//
//  Created by mrinal khullar on 4/20/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import "SubmitRequstVC.h"
//#import "FFCircularProgressView.h"
@interface SubmitRequstVC ()
{
    int SelectedCategory;
    UIActivityIndicatorView *activityIndicator;
    
    NSMutableArray *categories;
}
@end

@implementation SubmitRequstVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.SelectCategoryTableView setHidden:YES];
    
   // UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideCategoryView)];
   // [self.view addGestureRecognizer:tap];

    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Name"]);
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.organclubhouse.com/api/?option=get_category_list"]];
    NSData* data = [NSData dataWithContentsOfURL:url];
    if(data != nil){
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if(lookup != nil){
            
            categories = [NSMutableArray arrayWithArray:lookup];
            [self.SelectCategoryTableView reloadData];
        }
    }

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)SelectCategory:(id)sender {
    
    if(self.SelectCategoryTableView.isHidden){
        
        [(UIImageView*)[self.view viewWithTag:5] setImage:[UIImage imageNamed:@"keyboard-up-arrow-button"]];
        
        [UIView transitionWithView:self.SelectCategoryTableView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.SelectCategoryTableView setHidden:NO];
                        }
                        completion:NULL];
        
        
    }
    else{
        [(UIImageView*)[self.view viewWithTag:5] setImage:[UIImage imageNamed:@"keyboard-down-arrow-button"]];
        [UIView transitionWithView:self.SelectCategoryTableView
                          duration:0.4
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.SelectCategoryTableView setHidden:YES];
                        }
                        completion:NULL];
    }
    
}
    
    
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        
        return  categories.count;
    }
    
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        
        UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"cell" ];
        
        
        if(cell== nil){
            
        }
        
    [(UILabel*)[cell viewWithTag:1]setText:[NSString stringWithFormat:@"%@",[[categories valueForKey:@"NAME"]objectAtIndex:indexPath.row]]];
    
         return cell;
    
    }
    
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        
        SelectedCategory = [[[categories valueForKey:@"ID"] objectAtIndex:indexPath.row] intValue];
        [(UIImageView*)[self.view viewWithTag:5] setImage:[UIImage imageNamed:@"keyboard-down-arrow-button"]];
        [self.selectCategoryBtn setTitle:[NSString stringWithFormat:@"%@",[[categories valueForKey:@"NAME"]objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
    
        [self.SelectCategoryTableView setHidden:YES];
    
    }
    
 
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];

    return  YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)submitRequest:(id)sender {
    
    if( ![[self.songName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString: @""] ){
        
        if(SelectedCategory !=0){
            
          BOOL result = [self sendSubmitRequest];
           
            if(result){
                
                UIAlertView *alertView1=[[UIAlertView alloc]initWithTitle:@"Request received" message:@"Thank You For Your Submission." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView1  setTag:2];
                [alertView1 show];
                
            }
            else{
                [self showAlert:@"Error occured during submission of request."];
            }
        }
        else{
            [self showAlert:@"Select Category"];
        }
    }
    else{
        [self showAlert:@"Video name can not be empty"];
    }
    
}

-(void)showViewForSelection{
    
}

-(BOOL)sendSubmitRequest{
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Name"]);
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.organclubhouse.com/api/?option=get_video_request&category=%d&videoname=%@&user_id=%@&artist=%@",SelectedCategory, [self.songName.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[NSUserDefaults standardUserDefaults] valueForKey:@"Name"],[self.artistName.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSData* data = [NSData dataWithContentsOfURL:url];
    if(data != nil){
        NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if([[lookup valueForKey:@"status"] isEqualToString:@"Success"]){
    
            return YES;
        }
        else{
        
            return NO;
        }
    }
    else{
        return NO;
        
    }
}

-(void)showAlert:(NSString*)withMessage{
    
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:withMessage delegate:self
                                            cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
   [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 2){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void)ShowIndicator{
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    [self.view addSubview: activityIndicator];
    [activityIndicator startAnimating];
    
}
-(void)StopIndicator{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    
}
-(void)hideCategoryView{
 
    [(UIImageView*)[self.view viewWithTag:5] setImage:[UIImage imageNamed:@"keyboard-down-arrow-button"]];
    [UIView transitionWithView:self.SelectCategoryTableView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.SelectCategoryTableView setHidden:YES];
                    }
                    completion:NULL];}
@end
