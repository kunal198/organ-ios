//
//  FakeModelBuilder.h
//  Example
//
//  Created by Robert Nash on 19/09/2015.
//  Copyright © 2015 Robert Nash. All rights reserved.
//

#import "MenuSection.h"

@interface FakeModelBuilder : NSObject

+(NSArray *)buildHomeListMenu:(NSMutableArray*)array;
+(NSArray *)buildFavoriteListMenu:(NSMutableArray*)array;

@end
