//
//  MyList.h
//  PianoClubHouse
//
//  Created by kingcode on 8/25/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyList : NSObject {
    
    NSMutableArray *m_favoriteList;
    NSMutableArray *m_purchasedList;
}

@property(nonatomic,retain)NSMutableArray *m_favoriteList;
@property(nonatomic,retain)NSMutableArray *m_purchasedList;
@property(nonatomic,retain)NSMutableArray *m_downloadedList;

+(MyList*)getInstance;
@end
