//
//  PCHVideoPlayerViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 10/15/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface PCHVideoPlayerViewController : MPMoviePlayerViewController

@end
