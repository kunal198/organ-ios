//
//  FilterCell.m
//  PianoClubHouse
//
//  Created by kingcode on 8/18/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell

@synthesize m_toggleBtn;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onToggleBtnClick:(id)sender {
    m_toggleBtn.selected = !m_toggleBtn.selected;
}

-(void)resetToggleBtn {
    m_toggleBtn.selected = NO;
}

@end
