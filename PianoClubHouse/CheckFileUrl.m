//
//  CheckFileUrl.m
//  PianoClubHouse
//
//  Created by Gerard Turman on 10/31/15.
//  Copyright © 2015 kingcode. All rights reserved.
//

#import "CheckFileUrl.h"

@implementation CheckFileUrl

-(void)checkUrl:(NSURL*)url
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                             timeoutInterval:60.0];
    connectionManager = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"%lld", response.expectedContentLength);
    int nStatusCode = [(NSHTTPURLResponse*)response statusCode];
    if(nStatusCode == 200)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ValidFileNotification" object:self];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"InValidFileNotification" object:self];
    }
}

@end
