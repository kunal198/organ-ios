//
//  DownListViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 9/4/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RRNCollapsableSectionTableViewController.h"

@interface DownListViewController : RRNCollapsableTableViewController

@property (strong, nonatomic) NSArray *menu;

@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (weak, nonatomic) IBOutlet UIView *m_navBar;
@property (weak, nonatomic) IBOutlet UILabel *m_sortType;
@property (weak, nonatomic) IBOutlet UILabel *m_lbCategory;
@property (weak, nonatomic) IBOutlet UIImageView *m_imgTableBack;

- (IBAction)onSortBtnClick:(id)sender;
- (IBAction)onRemoveDownload:(id)sender;
- (IBAction)onWatchOfflineBtnClick:(id)sender;
- (IBAction)onBackBtnClick:(id)sender;

@end

