//
//  LogoutViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 10/15/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogoutViewController : UIViewController <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *m_imgBack;

@end
