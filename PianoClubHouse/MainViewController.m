//
//  MainViewController.m
//  PianoClubHouse
//
//  Created by ; on 9/29/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "Common.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTabs];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marks - UITabBarControllerDelegate

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    
    appDelegate.previousScreen = [self renderImageFromView:self.view withRect:self.view.frame];
    NSArray* items = self.tabBar.items;
    
    appDelegate.previousTabIndex = appDelegate.currentTabIndex;
    appDelegate.currentTabIndex = [items indexOfObject:item];
}

- (UIImage *)renderImageFromView:(UIView *)view withRect:(CGRect)frame {
    // Create a new context the size of the frame
    UIGraphicsBeginImageContextWithOptions(frame.size, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Render the view
    [view.layer renderInContext:context];
    //[view drawRect:frame];
    
    // Get the image from the context
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Cleanup the context you created
    UIGraphicsEndImageContext();
    
    return renderedImage;
}

-(void) addTabs
{
    
    NSMutableArray *listOfViewControllers = [[NSMutableArray alloc] init];
    
    NSArray* vcArray1 = [NSArray arrayWithObjects:@"Home", @"Search", @"Favorites", @"Downloads", @"Logout", nil];
    NSArray* vcArray2 = [NSArray arrayWithObjects:@"Home", @"Support", @"Favorites", @"Purchases", @"Downloads", nil];
    
    NSArray* vcArray = nil;
    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.appMode == PREMIUM)
        vcArray = vcArray1;
    else
        vcArray = vcArray2;
    
    for (NSString* vcName in vcArray)
    {
        UIViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:vcName];
        [listOfViewControllers addObject:vc];
    }
    
    [self setViewControllers:listOfViewControllers animated:YES];
    
    NSArray* selectedImages1 = [NSArray arrayWithObjects:@"ic_home_selected", @"ic_search_selected", @"ic_favorite_selected", @"ic_down_selected", @"ic_logout_selected", nil];
    NSArray* unSelectedImages1 = [NSArray arrayWithObjects:@"home_unselected", @"search_btn_unsel", @"favorite_unselected", @"down_unselected", @"logout_unselected", nil];
    
//    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//    {
//        selectedImages1 = [NSArray arrayWithObjects:@"home_sel_ipad", @"favorite_sel_ipad", @"down_sel_ipad", @"support_btn_sel", @"logout_unsel_ipad", nil];
//        unSelectedImages1 = [NSArray arrayWithObjects:@"home_unsel_ipad", @"favorite_unsel_ipad", @"down_unsel_ipad", @"support_btn_unsel", @"logout_unsel_ipad", nil];
//    }
    
    NSArray* selectedImages2 = [NSArray arrayWithObjects:@"ic_home_selected", @"search_btn_sel", @"ic_favorite_selected", @"purchase_selected", @"ic_down_selected", nil];
    NSArray* unSelectedImages2 = [NSArray arrayWithObjects:@"home_unselected", @"search_btn_unsel", @"favorite_unselected", @"purchase_unselected", @"down_unselected", nil];
    
    NSArray* selectedImages = nil;
    NSArray* unSelectedImages = nil;
    
//    if(appDelegate.appMode == PREMIUM)
//    {
//        selectedImages = selectedImages1;
//        unSelectedImages = unSelectedImages1;
//    }
//    else
//    {
//        selectedImages = selectedImages2;
//        unSelectedImages = unSelectedImages2;
//    }
    
    selectedImages = selectedImages1;
    unSelectedImages = unSelectedImages1;
    
    NSArray* items = self.tabBar.items;
    for(int i = 0; i < items.count; i++)
    {
        UITabBarItem * item = [items objectAtIndex:i];
        UIImage* selectedImage = [[UIImage imageNamed:[selectedImages objectAtIndex:i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIImage* unSelectedImage = [[UIImage imageNamed:[unSelectedImages objectAtIndex:i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
       // item.selectedImage = selectedImage;
        item.image = unSelectedImage;
        [item setTitlePositionAdjustment:UIOffsetMake(0, -5)];
        
    }
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    
    //colorWithRed:37/255.f green:14/255.f blue:50/255.f alpha:1
}

@end
