//
//  ListCell.m
//  PianoClubHouse
//
//  Created by kingcode on 10/9/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "ListCell.h"
#import "MyList.h"
#import "VideoData.h"
#import "AppDelegate.h"
#import "Common.h"
#import "G_Functions.h"
#import "MKStoreManager.h"

@interface ListCell ()
{
    BOOL m_bIsDownloading;
    UIActivityIndicatorView *m_Spinner;
}

@end

@implementation ListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onFavoriteBtnClick:(id)sender {
    
    if([sender tag] == 0)   //remove
    {
        UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Remove this favorite?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [confirmDlg show];
    }
    else
    {
        [self addToFavoriteList];
        self.m_btnSelectedFav.hidden = NO;
        self.m_btnUnSelectedFav.hidden = YES;
    }
}

-(void)removeFavorite{
    
    [self removeFromFavoriteList];
    self.m_btnSelectedFav.hidden = YES;
    self.m_btnUnSelectedFav.hidden = NO;
}

-(void)addToFavoriteList
{
    MyList* g_list = [MyList getInstance];
    if(g_list.m_favoriteList == nil)
        g_list.m_favoriteList = [[NSMutableArray alloc] init];
    [g_list.m_favoriteList addObject:[self getVideoData]];
}

-(void)removeFromFavoriteList
{
    MyList* g_list = [MyList getInstance];
    for (int i=0;i<[g_list.m_favoriteList count]; i++) {
        VideoData *item = [g_list.m_favoriteList objectAtIndex:i];
        if ([item.video_id isEqualToString:self.video_id]) {
            [g_list.m_favoriteList removeObject:item];
            i--;
            return;
        }
    }
}

-(void)saveToDownloadedList
{
    MyList* g_list = [MyList getInstance];
    if(g_list.m_downloadedList == nil)
        g_list.m_downloadedList = [[NSMutableArray alloc] init];
    [g_list.m_downloadedList addObject:[self getVideoData]];
    
    NSLog(@"%@ video saved to download list", self.video_id);
}

-(void)saveToPurchasedList
{
    MyList* g_list = [MyList getInstance];
    if(g_list.m_purchasedList == nil)
        g_list.m_purchasedList = [[NSMutableArray alloc] init];
    [g_list.m_purchasedList addObject:[self getVideoData]];
    
    NSLog(@"%@ video saved to purchased list", self.video_id);
}

-(void)removeFromDownloadedList
{
    MyList* g_list = [MyList getInstance];
    for (int i=0;i<[g_list.m_downloadedList count]; i++) {
        VideoData *item = [g_list.m_downloadedList objectAtIndex:i];
        if ([item.video_id isEqualToString:self.video_id]) {
            [g_list.m_downloadedList removeObject:item];
            i--;
            return;
        }
    }
}

-(VideoData*)getVideoData
{
    VideoData* pData = [[VideoData alloc] init];
    
    pData.video_id = self.video_id;
    pData.title = self.title;
    pData.artist = self.artist;
    pData.previewID = self.preview_id;
    pData.video_length = self.m_lbVideoLength.text;
    pData.publish_date = self.m_lbPublishDate.text;
    pData.instructor = self.m_lbInstructor.text;
    pData.song_key = self.m_lbSongKey.text;
    pData.mp4_name = self.mp4_name;
    
    return pData;
}

-(void)refreshFavorites
{
    if([self checkInFavoritesList:self.video_id])
    {
        self.m_btnSelectedFav.hidden = NO;
        self.m_btnUnSelectedFav.hidden = YES;
    }
    else
    {
        self.m_btnSelectedFav.hidden = YES;
        self.m_btnUnSelectedFav.hidden = NO;
    }
}

-(void)refreshDownloads
{
    self.m_btnPreview.hidden = YES;
    self.m_btnPurchase.hidden = YES;
    
    if([self checkInDownloadedList:self.video_id])
    {
        self.m_btnWatchOffline.hidden = NO;
        self.m_lbDownloaded.hidden = NO;
        self.m_deleteBtn.hidden = NO;
        self.m_btnWatch.hidden = YES;
        self.m_btnDownload.hidden = YES;
    }
    else
    {
        self.m_btnWatchOffline.hidden = YES;
        self.m_lbDownloaded.hidden = YES;
        self.m_deleteBtn.hidden = YES;
        self.m_btnWatch.hidden = NO;
        if(m_bIsDownloading)
            self.m_btnDownload.hidden = YES;
        else
            self.m_btnDownload.hidden = NO;
    }
}


-(void)GuestUser{
    
    self.m_btnWatch.hidden = NO;
    
    self.m_btnWatchOffline.hidden = YES;
    self.m_btnPreview.hidden = YES;
    self.m_btnPurchase.hidden = YES;
    self.m_btnDownload.hidden= YES;
    self.m_deleteBtn.hidden = YES;
    self.m_btnWatchOffline.hidden = YES;
    self.m_btnUnSelectedFav.hidden= YES;
    self.m_btnSelectedFav.hidden=YES;
    

}


-(void)refresh
{
    
    
    if([G_Functions IsGuestMode])
    {
        [self GuestUser];
    }
    else{
        
        [self refreshFavorites];
        
        if([G_Functions isPremiumMode])
        {
            [self refreshDownloads];
        }
        else       //standard
        {
            if(![self checkInPurchaseList:self.video_id] && ![self checkInDownloadedList:self.video_id])
            {
                self.m_btnWatchOffline.hidden = YES;
                self.m_lbDownloaded.hidden = YES;
                self.m_btnWatch.hidden = YES;
                self.m_btnPreview.hidden = NO;
                self.m_btnPurchase.hidden = NO;
            }
            else    //check downloaded
            {
            [self refreshDownloads];
            }
        }
    }
}

-(BOOL)checkInFavoritesList:(NSString*)videoID
{
    MyList* g_list = [MyList getInstance];
    for (VideoData *iterator in g_list.m_favoriteList) {
        if([iterator.video_id isEqualToString:videoID])   //if it is in favorite list
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)checkInDownloadedList:(NSString*)videoID
{
    MyList* g_list = [MyList getInstance];
    for (VideoData *iterator in g_list.m_downloadedList) {
        if([iterator.video_id isEqualToString:videoID])   //if it is in downloaded list
        {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)checkInPurchaseList:(NSString*)videoID
{
    MyList* g_list = [MyList getInstance];
    for (VideoData *iterator in g_list.m_purchasedList) {
        if([iterator.video_id isEqualToString:videoID])   //if it is in purchased list
        {
            return YES;
        }
    }
        
    return NO;
}

- (IBAction)onPurchaseBtnClick:(id)sender
{
    //    if(m_Spinner == nil)
    //        m_Spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //    [m_Spinner setCenter:CGPointMake(100, 50)];
    //    [self.superview addSubview:m_Spinner];
    //    [m_Spinner startAnimating];
    
    [[MKStoreManager sharedManager] setSender:self];
    [[MKStoreManager sharedManager] buyVideo];
}

- (IBAction)onDeleteBtnClick:(id)sender
{
    UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Confirm removal of this download?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    [confirmDlg show];
}

-(void)deleteVideo
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.mp4_name];
    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [removeSuccessFulAlert show];
        [self removeFromDownloadedList];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }
    
    [self refresh];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
        {
            if([[alertView buttonTitleAtIndex:1] isEqualToString:@"Yes"])
                [self removeFavorite];
            else
                [self deleteVideo];
            
            break;
        }
    }
}

@end
