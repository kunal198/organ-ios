//
//  G_Filters.m
//  PianoClubHouse
//
//  Created by kingcode on 9/9/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "G_Filters.h"

@implementation G_Filters

@synthesize m_genreFilters;
@synthesize m_keyFilters;
@synthesize m_instructorFilters;

static G_Filters *instance = nil;

+(G_Filters *)getInstance
{
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [G_Filters new];
        }
    }
    return instance;
}

@end
