//
//  BrowseCell.h
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title_artist;
@property (weak, nonatomic) IBOutlet UILabel *detail_info;

@property (weak, nonatomic) NSString *video_id;

@end
