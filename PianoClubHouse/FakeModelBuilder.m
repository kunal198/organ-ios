//
//  FakeModelBuilder.m
//  Example
//
//  Created by Robert Nash on 19/09/2015.
//  Copyright © 2015 Robert Nash. All rights reserved.
//

#import "FakeModelBuilder.h"
#import "VideoData.h"

@implementation FakeModelBuilder

+(NSArray *)buildHomeListMenu:(NSMutableArray*)array {
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < array.count; i++) {
        
        MenuSection *section = [MenuSection new];
        
        NSDictionary* dic = [array objectAtIndex:i];
        NSString* videoTitle = [dic objectForKey:@"VideoTitle"];
        if([videoTitle isKindOfClass:[NSNull class]])
        {
            videoTitle = @"";
        }
        
        NSString* videoArtist = [dic objectForKey:@"VideoArtist"];
        if([videoArtist isKindOfClass:[NSNull class]])
        {
            videoArtist = @"";
        }
        
        section.title = videoTitle;
        section.artist = [NSString stringWithFormat:@"(%@)", videoArtist];
        section.items = @[[NSNull null]];
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

+(NSArray *)buildFavoriteListMenu:(NSMutableArray*)array {
    
    NSMutableArray *collector = [NSMutableArray new];
    
    for (NSInteger i = 0; i < array.count; i++) {
        
        MenuSection *section = [MenuSection new];
        
        VideoData* data = [array objectAtIndex:i];
        
        section.title = data.title;
        section.artist = [NSString stringWithFormat:@"(%@)", data.artist];
        section.items = @[[NSNull null]];
        
        [collector addObject:section];
    }
    
    return [collector copy];
}

@end
