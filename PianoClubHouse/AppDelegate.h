//
//  AppDelegate.h
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

#define appdelegate ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign) BOOL fullScreenVideoIsPlaying;
@property (nonatomic,assign) NSInteger appMode;
@property (retain, nonatomic) NSString *g_UserName;
@property (retain, nonatomic) NSString *g_Email;
@property (nonatomic,assign) NSInteger previousTabIndex;
@property (nonatomic, assign) NSInteger currentTabIndex;
@property (strong, nonatomic) UIImage *previousScreen;
@property (strong, nonatomic) NSMutableArray *m_arrSelectedInstructors;

@end

