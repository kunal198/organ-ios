//
//  UIButton+Tagged.h
//  PianoClubHouse
//
//  Created by kingcode on 10/14/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Tagged)

@property (nonatomic, copy) NSString *stringTag;

@end
