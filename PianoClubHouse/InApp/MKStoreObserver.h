
#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "ListCell.h"

@interface MKStoreObserver : NSObject<SKPaymentTransactionObserver> {
    
    id m_sender;
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions;
- (void) failedTransaction: (SKPaymentTransaction *)transaction;
- (void) completeTransaction: (SKPaymentTransaction *)transaction;
- (void) restoreTransaction: (SKPaymentTransaction *)transaction;
- (void) setSender: (id)sender;

@end
