//
//  KeyFilterViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/22/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "KeyFilterViewController.h"
#import "SearchViewController.h"
#import "G_Functions.h"
#import "Common.h"

@interface KeyFilterViewController ()

@end

#define KEY_COUNT 13
@implementation KeyFilterViewController
@synthesize m_navbar;
@synthesize m_arrSelected;
@synthesize m_selectedKeys;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

  //  self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"search_back"]];
    
    if(IDIOM == IPAD)
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navbar_ipad"]];
    else
        m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];

    m_arrSelected = [[NSMutableArray alloc]init];
    for (int i = 0; i < KEY_COUNT; ++ i)
        [m_arrSelected addObject:[NSNumber numberWithInt:0]];
    
    [self initFilters];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)initFilters
{
    for(UIView *subview in self.self.view.subviews)
    {
        if([subview isKindOfClass:[UIButton class]])
        {
            UIButton* btn = (UIButton*)subview;
            if([m_selectedKeys indexOfObject:[NSNumber numberWithInteger:[subview tag]]] != NSNotFound)
            {
                btn.selected = YES;
                m_arrSelected[[subview tag]] = [NSNumber numberWithInt:1];
            }
            else
            {
                btn.selected = NO;
                m_arrSelected[[subview tag]] = [NSNumber numberWithInt:0];
            }
        }
    }
}

- (IBAction)onBackBtnClick:(id)sender {
    
    SearchViewController* pBackView = (SearchViewController*)[self backViewController];
    pBackView.m_keyFilters = [self getSelectedKeys];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)backViewController
{
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    
    if (numberOfViewControllers < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:numberOfViewControllers - 2];
}

- (IBAction)onFilter:(id)sender {
    UIButton* pButton = (UIButton*)sender;
    pButton.selected = !pButton.selected;
    
    if(pButton.selected)
        m_arrSelected[[sender tag]] = [NSNumber numberWithInt:1];
    else
        m_arrSelected[[sender tag]] = [NSNumber numberWithInt:0];
    
}

- (IBAction)onResetBtnClick:(id)sender {
    UIButton* currentButton = (UIButton*)sender;
    for (UIView *button in currentButton.superview.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
            m_arrSelected[[button tag]] = [NSNumber numberWithInt:0];
        }
    }
}

-(NSMutableArray*)getSelectedKeys
{
    NSMutableArray* arrKeys = [[NSMutableArray alloc]init];
    for(int i = 0; i < KEY_COUNT; ++i)
    {
        NSNumber *temp = [m_arrSelected objectAtIndex:i];
        if([temp intValue] == 1)
            [arrKeys addObject:[NSNumber numberWithInt:i]];
    }
    
    return arrKeys;
}
@end
