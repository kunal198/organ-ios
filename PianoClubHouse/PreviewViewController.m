//
//  PreviewViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 8/19/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "PreviewViewController.h"

@interface PreviewViewController ()

@end

@implementation PreviewViewController

@synthesize m_pWebView;
@synthesize m_sPreviewID;
@synthesize m_navbar;
@synthesize m_lbTitle;
@synthesize m_sTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"previewID = %@", m_sPreviewID);
    [self playVideoPreview];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"detail_back"]];
    m_navbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"browse_title"]];
    m_lbTitle.text = m_sTitle;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)playVideoPreview
{
    NSString *videoURL = [NSString stringWithFormat:@"http://www.youtube.com/v/%@&hl=en&rel=0&showsearch=0", m_sPreviewID];
    NSLog(@"videoURL === %@", videoURL);
    [self embedYouTube:videoURL frame:CGRectMake(0, 0, 320, 200)];
}

- (void)embedYouTube:(NSString*)url frame:(CGRect)frame {
    NSString* embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
        background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    NSString* html = [NSString stringWithFormat:embedHTML, url, m_pWebView.frame.size.width, m_pWebView.frame.size.height];
    
    NSLog(@"html === %@", html);
    [m_pWebView loadHTMLString:html baseURL:nil];
}

- (IBAction)onBackBtnClick:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
