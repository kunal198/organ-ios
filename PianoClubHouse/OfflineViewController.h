//
//  OfflineViewController.h
//  PianoClubHouse
//
//  Created by Gerard Turman on 11/13/15.
//  Copyright © 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfflineViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *m_btnContinue;
- (IBAction)onBtnContinueClick:(id)sender;

@end
