//
//  SearchViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/18/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) NSDictionary *m_dicFilterKeys;
@property (weak, nonatomic) IBOutlet UIView *m_navbar;

@property (strong , nonatomic) NSMutableArray *m_instructorFilters;
@property (strong , nonatomic) NSMutableArray *m_keyFilters;
@property (strong , nonatomic) NSMutableArray *m_genreFilters;
@property (strong , nonatomic) NSMutableArray *m_keywords;

@property (weak, nonatomic) IBOutlet UITextField *m_txtKeyword;
@property (weak, nonatomic) IBOutlet UILabel *m_lbKeyFilters;
@property (weak, nonatomic) IBOutlet UILabel *m_lbGenreFilters;
@property (weak, nonatomic) IBOutlet UILabel *m_lbInstructorFilters;
@property (weak, nonatomic) IBOutlet UIScrollView *m_scrollView;

- (IBAction)onBackBtnClick:(id)sender;
- (IBAction)onDisplayBtnClick:(id)sender;
- (IBAction)onResetAllBtnClick:(id)sender;

@end
