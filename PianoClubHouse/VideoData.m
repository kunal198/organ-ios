//
//  VideoData.m
//  PianoClubHouse
//
//  Created by kingcode on 8/16/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "VideoData.h"

@implementation VideoData

@synthesize video_id;
@synthesize title;
@synthesize artist;
@synthesize publish_date;
@synthesize video_length;
@synthesize instructor;
@synthesize song_key;
@synthesize previewID;
@synthesize mp4_name;

- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:video_id forKey:@"id"];
    [coder encodeObject:title forKey:@"title"];
    [coder encodeObject:artist forKey:@"artist"];
    [coder encodeObject:publish_date forKey:@"publish_date"];
    [coder encodeObject:video_length forKey:@"video_length"];
    [coder encodeObject:instructor forKey:@"instructor"];
    [coder encodeObject:song_key forKey:@"song_key"];
    [coder encodeObject:mp4_name forKey:@"mp4_name"];
    [coder encodeObject:previewID forKey:@"previewID"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [super init];
    if (self != nil)
    {
        video_id = [coder decodeObjectForKey:@"id"];
        title = [coder decodeObjectForKey:@"title"];
        artist = [coder decodeObjectForKey:@"artist"];
        publish_date = [coder decodeObjectForKey:@"publish_date"];
        video_length = [coder decodeObjectForKey:@"video_length"];
        instructor = [coder decodeObjectForKey:@"instructor"];
        song_key = [coder decodeObjectForKey:@"song_key"];
        mp4_name = [coder decodeObjectForKey:@"mp4_name"];
        previewID = [coder decodeObjectForKey:@"previewID"];
    }
    return self;
}

@end
