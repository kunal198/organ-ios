//
//  FilterCell.h
//  PianoClubHouse
//
//  Created by kingcode on 8/18/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *filterName;
@property (weak, nonatomic) IBOutlet UIButton *m_toggleBtn;

- (IBAction)onToggleBtnClick:(id)sender;
- (void)resetToggleBtn;

@end
