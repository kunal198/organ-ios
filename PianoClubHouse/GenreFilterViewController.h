//
//  GenreFilterViewController.h
//  PianoClubHouse
//
//  Created by kingcode on 8/24/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenreFilterViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *m_tableView;
@property (strong, nonatomic) NSMutableArray *m_arrGenres;
@property (strong, nonatomic) NSMutableDictionary *m_dicGenres;
@property (weak, nonatomic) IBOutlet UIView *m_navbar;
@property (strong, nonatomic) NSMutableArray *m_arrSelected;
@property (weak, nonatomic) IBOutlet UIButton *m_btnReset;

- (IBAction)onBackBtnClick:(id)sender;
- (IBAction)onResetBtnClick:(id)sender;

@end
