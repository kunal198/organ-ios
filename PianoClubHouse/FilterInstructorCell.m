//
//  FilterInstructorCell.m
//  PianoClubHouse
//
//  Created by Eden on 2/10/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import "FilterInstructorCell.h"
#import "AppDelegate.h"

@implementation FilterInstructorCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)onToggleBtnClick:(id)sender {
    [super onToggleBtnClick:sender];
    if(self.m_toggleBtn.selected)
        [appdelegate.m_arrSelectedInstructors addObject:[NSNumber numberWithInteger: self.m_toggleBtn.tag]];
    else
        [appdelegate.m_arrSelectedInstructors removeObject:[NSNumber numberWithInteger: self.m_toggleBtn.tag]];
}

@end
