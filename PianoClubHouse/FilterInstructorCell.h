//
//  FilterInstructorCell.h
//  PianoClubHouse
//
//  Created by Eden on 2/10/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import "FilterCell.h"

@interface FilterInstructorCell : FilterCell

- (IBAction)onToggleBtnClick:(id)sender;

@end
