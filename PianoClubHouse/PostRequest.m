//
//  PostRequest.m
//  PianoClubHouse
//
//  Created by kingcode on 8/24/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "PostRequest.h"

@implementation PostRequest

+(NSString *)postDataToUrl:(NSString*)urlString jsonString:(NSString*)jsonString
{
    NSData* responseData = nil;
    NSURL *url=[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    responseData = [NSMutableData data] ;
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    NSString *bodydata = [NSString stringWithFormat:@"data=%@",jsonString];
    NSData *req = [NSData dataWithBytes:[bodydata UTF8String] length:[bodydata length]];
    [request setHTTPBody:req];
    
    NSURLResponse* response;
    
    NSError* error = nil;
    responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"the final output is:%@",responseString);
    
    return responseString;
}

@end
