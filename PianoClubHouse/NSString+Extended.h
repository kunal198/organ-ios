//
//  NSString+Extended.h
//  PianoClubHouse
//
//  Created by Eden on 2/23/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extended)

- (NSString *)urlencode;

@end
