//
//  G_Functions.h
//  PianoClubHouse
//
//  Created by kingcode on 10/17/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VideoData.h"

@interface G_Functions : NSObject

+(BOOL)isPremiumMode;
+(BOOL)IsGuestMode;
+(NSString*)getSafeString:(NSString*)str;
+(void)saveToPurchasedList:(VideoData*)data;
+(uint64_t)getFreeDiskspace;

@end
