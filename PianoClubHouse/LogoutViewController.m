//
//  LogoutViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 10/15/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "LogoutViewController.h"
#import "LoginViewController.h"
#import "MainNavViewController.h"
#import "AppDelegate.h"
#import "G_Functions.h"
@implementation LogoutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    [self.m_imgBack setImage:app.previousScreen];
     
    UIAlertView *confirmDlg=[[UIAlertView alloc]initWithTitle:@"OrganClubHouse" message:@"Do you want to logout?" delegate:self
                                            cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [confirmDlg show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            [self backToPreviousTabbar];
            break;
        case 1: //"Yes" pressed
            [self navigateToLogin];
            break;
    }
}

- (void)navigateToLogin
{
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"search_back"]];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FullName"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Pwd"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Date"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Date"];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isGuestUser"];
     [G_Functions IsGuestMode];
     
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    app.previousTabIndex = 0;
    app.currentTabIndex = 0;
    app.appMode = 0;
    
    BOOL isExist = NO;
    
    for (id controller in [self.navigationController viewControllers])
    {
        if ([controller isKindOfClass:[LoginViewController class]])
        {
            [self.navigationController popToViewController:controller animated:YES];
            isExist = YES;
            break;
        }
    }
    
    if(!isExist)
    {
        LoginViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"login_view"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    
    
}

- (void)backToPreviousTabbar
{
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSInteger temp = app.currentTabIndex;

    [self.tabBarController setSelectedIndex:app.previousTabIndex];
    app.currentTabIndex = app.previousTabIndex;
    app.previousTabIndex = temp;
}

@end
