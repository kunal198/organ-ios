//
//  PCHVideoPlayerViewController.m
//  PianoClubHouse
//
//  Created by kingcode on 10/15/15.
//  Copyright (c) 2015 kingcode. All rights reserved.
//

#import "PCHVideoPlayerViewController.h"
#import "PCHMaskView.h"
#import "Common.h"

@interface PCHVideoPlayerViewController ()
{
    PCHMaskView* m_maskPortrait;
    PCHMaskView* m_maskLandscape;
    
    BOOL m_bAddPortrait;
    BOOL m_bAddLandscape;
}

@end

@implementation PCHVideoPlayerViewController

-(void)viewDidLoad
{
    m_bAddPortrait = NO;
    m_bAddLandscape = NO;
    
    [self addLogoMask];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:[UIDevice currentDevice]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MPMoviePlayerPlaybackStateDidChange:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:nil];
    [self disableZoomVideo];
}

-(void)disableZoomVideo
{
    [[[self.moviePlayer view] subviews] enumerateObjectsUsingBlock:^(id view, NSUInteger idx, BOOL *stop) {
        [[view gestureRecognizers] enumerateObjectsUsingBlock:^(id tap, NSUInteger idx, BOOL *stop) {
            if([tap isKindOfClass:[UITapGestureRecognizer class]]) {
                
                if([tap numberOfTapsRequired]==2)
                {
                    [view removeGestureRecognizer:tap];
                    
                }
            }
        }];
    }];
}

- (void) orientationChanged:(NSNotification *)note
{
    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            [self showLogoMask:YES];
            break;
            
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            [self showLogoMask:NO];
            break;
            
        default:
            break;
    };
}

-(void)MPMoviePlayerPlaybackStateDidChange:(NSNotification *)notification
{
    if(self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying)
    { //playing
        NSLog(@"Start Playing");
        if([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationPortrait)
            [self showLogoMask:YES];
        else if([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight)
            [self showLogoMask:NO];
    }
    
    if(self.moviePlayer.playbackState == MPMoviePlaybackStateStopped)
    { //stopped
        NSLog(@"Stop Playing");
    }

}

-(void)showLogoMask:(BOOL)bIsPortrait
{
    if(bIsPortrait)
    {
        if(!m_bAddPortrait)
        {
            [self.moviePlayer.view addSubview:m_maskPortrait];
            m_bAddPortrait = YES;
        }
        m_maskPortrait.hidden = NO;
        m_maskLandscape.hidden = YES;
    }
    else
    {
        if(!m_bAddLandscape)
        {
            [self.moviePlayer.view addSubview:m_maskLandscape];
            m_bAddLandscape = YES;
        }
        m_maskPortrait.hidden = YES;
        m_maskLandscape.hidden = NO;
    }
}


-(void)addLogoMask
{
    m_maskPortrait = [[PCHMaskView new]init];
    m_maskLandscape = [[PCHMaskView new]init];
    
    UIImage *logo_portrait = IDIOM == IPAD? [UIImage imageNamed: @"mask_portrait_ipad"] : [UIImage imageNamed: @"mask_portrait"];
    [self setMaskBackgroundToView:m_maskPortrait withBack:logo_portrait];
    
    UIImage *logo_landscape = IDIOM == IPAD? [UIImage imageNamed: @"mask_landscape_ipad"] : [UIImage imageNamed: @"mask_landscape"];
    [self setMaskBackgroundToView:m_maskLandscape withBack:logo_landscape];
}

-(void)setMaskBackgroundToView:(UIView*) view withBack:(UIImage*)imgBack
{
    view.frame = CGRectMake(0, 0, imgBack.size.width, imgBack.size.height);
    view.backgroundColor = [UIColor colorWithPatternImage:imgBack];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

@end
