//
//  WarningViewController.m
//  PianoClubHouse
//
//  Created by mrinal khullar on 4/20/17.
//  Copyright © 2017 kingcode. All rights reserved.
//

#import "WarningViewController.h"

@interface WarningViewController ()
{
    UIButton *button;
}
@end

@implementation WarningViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *appVersion = [[NSUserDefaults standardUserDefaults]objectForKey:@"updateDetail_key"];
    
    if ( [appVersion isEqualToString:@"update"]) {
        
        if([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppUnderMaintenance"] isEqualToString:@"YES"]){
            
            UILabel *l =[self.view viewWithTag:1];
            [l setNumberOfLines:5];
            [l setText:@"We are sorry, but the app is currently undergoing routine maintenance. Please check back at a later time"];
            button = [self.view viewWithTag:2] ;
            [button setTitle:@"Check Later" forState:UIControlStateNormal];
        }
        
        else{
            UILabel *l =[self.view viewWithTag:1];
            [l setText:@"Good news! A new version of the app is available."];
            
        }
    }
    

    
    
    
}
- (IBAction)buttonPressed:(id)sender {
    
    if([button.titleLabel.text isEqualToString:@"Check Later"]){
        exit(0);
        
    }
    else{
        
        NSString *iTunesLink = @"https://itunes.apple.com/in/app/pianoclubhouse-premium/id1063098413?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
